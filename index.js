const firstNameValue = document.querySelector('#txt-first-name');
const lastNameValue = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

firstNameValue.addEventListener('keyup', printFullName);
lastNameValue.addEventListener('keyup', printFullName);

function printFullName(e){
    spanFullName.innerHTML = firstNameValue.value + ' ' + lastNameValue.value;
};

